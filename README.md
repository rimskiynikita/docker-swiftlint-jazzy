# docker-swiftlint-jazzy

Docker container based on Alpine Linux containing CocoaPods, SwiftLint and Jazzy.  [cloud.docker.com/repository/docker/rimskiynikita/swiftlint-jazzy](https://cloud.docker.com/repository/docker/rimskiynikita/swiftlint-jazzy).
