FROM swift

# Author
MAINTAINER Rimskiy Nikita

# Install ruby
RUN apt-get update && apt-get install -y \
    libsqlite3-dev ruby-dev && \
    rm -r /var/lib/apt/lists/*

ENV SWIFTLINT_REVISION="master"

# Install SwiftLint
RUN git clone --branch $SWIFTLINT_REVISION https://github.com/realm/SwiftLint.git && \
    cd SwiftLint && \
    swift build --configuration release --static-swift-stdlib && \
    mv `swift build --configuration release --static-swift-stdlib --show-bin-path`/swiftlint /usr/bin && \
    cd .. && \
    rm -rf SwiftLint

ENV SOURCEKITTEN_REVISION="0.23.2"
ENV JAZZY_VERSION=""

# Install SourceKitten and Jazzy
RUN git clone --branch $SOURCEKITTEN_REVISION https://github.com/jpsim/SourceKitten.git && \
    cd SourceKitten && \
    swift build --configuration release --static-swift-stdlib && \
    mv `swift build --configuration release --static-swift-stdlib --show-bin-path`/sourcekitten /usr/bin && \
    cd .. && \
    rm -rf SourceKitten && \
    gem install bundler && \
    gem install jazzy --version "$JAZZY_VERSION"